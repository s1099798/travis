FROM microsoft/dotnet:2.1-sdk AS build-env
WORKDIR /app

ENV ASPNETCORE_ENVIRONMENT Development

RUN curl -sL https://deb.nodesource.com/setup_10.x |  bash -
RUN apt-get install -y nodejs

COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN dotnet publish -c Release -o output

# Runtime image
FROM microsoft/dotnet:2.1-sdk
WORKDIR /app
EXPOSE 80

ENV ASPNETCORE_ENVIRONMENT Production

COPY --from=build-env /app/output .
ENTRYPOINT ["dotnet", "travis.dll"]

# docker build --rm -f "Travis\Production.Dockerfile" -t travis:2.1 Travis
# docker run --rm -it -p 80:80 travis:2.1